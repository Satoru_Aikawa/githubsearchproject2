//
//  User.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/09.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

struct User: Decodable {
    let login: String
    let id: Int
}
