//
//  Repository.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/09.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

struct Repository: Decodable {
    let id: Int
    let name: String
    let fullName: String
    let owner: User
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case owner
    }
}
