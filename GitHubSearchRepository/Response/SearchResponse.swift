//
//  SearchResponse.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/10.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

struct SearchResponse<Item: Decodable>: AbstructResponse {
    typealias Item = Item
    
    let totalCount: Int
    let items: [Item]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case items
    }
}

protocol AbstructResponse: Decodable {
    associatedtype Item: Decodable
    
    var totalCount: Int {get}
    var items: [Item] {get}
}
