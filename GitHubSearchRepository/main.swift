//
//  main.swift
//  GitHubSearchRepository
//
//  Created by 999-503 on 2019/11/08.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

//Function to send request
func sendClient<T: GitHubRequest>(request: T) {
    let client = GitHubClient()
    
    client.send(request: request, completion: {(result) in
        switch result {
        case let .success(response):
            response.items.forEach {print($0)}
            exit(0)
        case let .failure(error):
            print(error)
            exit(1)
        }
    })
}

//Entry point of whole programm

var category: String? = nil

//Prompt to input character until valid one is typed.
while(category == nil) {
    print("Is what you would like to search is Repository? (1: Repository /2: User)")
    
    let tmpCharacter = readLine(strippingNewline: true)
    if ["1", "2"].contains(tmpCharacter) {
        category = tmpCharacter
    }
}

print("Please input search keyword: ")

guard let keyWord = readLine(strippingNewline: true) else {
    exit(1)
}

let client = GitHubClient()

switch category {
case "1":
    let request = GitHubAPI.SearchRepositories(keyword: "\(keyWord)")
    
    sendClient(request: request)
case "2":
    let request = GitHubAPI.SearchUsers(keyWord: "\(keyWord)")
    
    sendClient(request: request)
    
default:
    fatalError("Unexpected category is detected.")
}




//Time out
let timeoutInterval: TimeInterval = 300

Thread.sleep(forTimeInterval: timeoutInterval)

//タイムアウト後の処理
print("Connection timeout")
exit(1)
