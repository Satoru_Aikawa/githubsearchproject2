//
//  GitHubRequest.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/16.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

protocol GitHubRequest {
    associatedtype Response: AbstructResponse

    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var queryItems: [URLQueryItem] { get }
}

extension GitHubRequest {
    var baseURL: URL {
        return URL(string: "https://api.github.com")!
    }
}

//function to create URLRequest to pass to URLSession class.
extension GitHubRequest {
    func buildURLRequest() -> URLRequest {
        let url = self.baseURL.appendingPathComponent(self.path, isDirectory: false)
        
        //Use URLConponents to combine parts into one URL.
        var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        switch self.httpMethod {
        case .get:
            components?.queryItems = self.queryItems
        default:
            //only Get method is expected.
            fatalError("Unsupported method: \(httpMethod)")
        }
        
        //Create URLRequest
        var urlRequest = URLRequest(url: components?.url ?? url)
        urlRequest.httpMethod = self.httpMethod.rawValue
        
        return urlRequest
    }
}

extension GitHubRequest {
    func response(from data: Data, urlResponse: URLResponse) throws -> Response {
        let decoder = JSONDecoder()
        
        //When status code is fine, returns associated Response type. If not, returns error.
        if case (200..<300)? = (urlResponse as? HTTPURLResponse)?.statusCode {
            
            //valid search result, or parse failure
            return try decoder.decode(Response.self, from: data)
        } else {
            //GitHub returned API error as it's contents.
            throw try decoder.decode(GitHubAPIError.self, from: data)
        }
    }
}
