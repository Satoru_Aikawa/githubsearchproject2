//
//  HTTPMethod.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/16.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
}
