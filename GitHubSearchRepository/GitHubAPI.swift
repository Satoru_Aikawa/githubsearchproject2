//
//  GitHubAPI.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/17.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

final class GitHubAPI {
    
    struct SearchRepositories: GitHubRequest {
        
        var path: String {
            return "/search/repositories"
        }
        
        var httpMethod: HTTPMethod {
            return .get
        }
        
        var queryItems: [URLQueryItem] {
            return [URLQueryItem(name: "q", value: self.keyword)]
        }
        
        typealias Response = SearchResponse<Repository>
        
        // The word you would like to search with.
        let keyword: String
    }
    
    struct SearchUsers: GitHubRequest {
        
        typealias Response = SearchResponse<User>
        
        var path: String {
            return "/search/users"
        }
        
        var httpMethod: HTTPMethod {
            .get
        }
        
        var queryItems: [URLQueryItem] {
            return [URLQueryItem(name: "q", value: self.keyWord)]
        }
        
        let keyWord: String
        
        
    }
    
}
