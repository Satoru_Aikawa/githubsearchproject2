//
//  GitHubAPIError.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/11.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

struct GitHubAPIError: Decodable, Error {
    
    struct FieldError: Decodable {
        let resource: String
        let filed: String
        let code: String
    }
    
    let message: String
    let errors: [FieldError]?
}
