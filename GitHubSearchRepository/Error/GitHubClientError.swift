//
//  GitHubClient.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/10.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

enum GitHubClientError: Error {
    //Failed to communicate
    case connectionError(Error)
    
    //Failed to trans
    case responseParseError(Error)
    
    //Got an error response from API
    case apiError(GitHubAPIError)
}
