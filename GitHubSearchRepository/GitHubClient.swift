//
//  GitHubClient.swift
//  GitHubSearchRepository
//
//  Created by USER on 2019/11/24.
//  Copyright © 2019 999-503. All rights reserved.
//

import Foundation

//Class to combine each components to communicate with GitHub server.
class GitHubClient {
    private let session = { () -> URLSession in
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        return session
    }()
    
    func send<Request: GitHubRequest>(request: Request, completion: @escaping (Result<Request.Response, GitHubClientError>) -> Void) {
        let urlRequest = request.buildURLRequest()
        let task = self.session.dataTask(with: urlRequest, completionHandler: {(data, response, error) in
            switch (data, response, error) {
            case (_, _, let error?):
            completion(Result.failure(GitHubClientError.connectionError(error)))
                return
            case (let data?, let response?, _) :
                do {
                    let response = try request.response(from: data, urlResponse: response)
                    completion(Result.success(response))
                    
                } catch let error as GitHubAPIError {
                    //GitHub returned error as it's contents
                    completion(Result.failure(GitHubClientError.apiError(error)))
                    
                } catch {
                    //GitHub response cannot be interpreted as meaningful data
                    completion(Result.failure(GitHubClientError.responseParseError(error)))
                }
            default:
                //Cannot reach here as long as URLSession class works.
                fatalError("Invalid response combination: data: \(String(describing: data)), response: \(String(describing: response)), error: \(String(describing: error))")
            }
            
            })
        
        task.resume()
    }
}
